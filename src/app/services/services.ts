import { Subject } from 'rxjs/Subject';
import { Post } from '../models/post.model';

export class PostService {

  postsSubject = new Subject<any[]>();

  private posts: Array<object> = [
    {
      title: 'Premier post',
      content: 'Je suis le 1er post, je suis arrivé d\'abord. C\'est moi qu\'il faut aimer le plus',
      loveIt: 1,
      created_at: new Date()
    },
    {
      title: 'le post numéro 2',
      content: 'Hey, moi je suis le post numéro 2, j\'ai pas grand chose à dire à part que je suis mieux que le 1',
      loveIt: 0,
      created_at: new Date()
    },
    {
      title: 'le Troisième Post',
      content: 'Parce que jamais deux sans trois, du coup me voilà.',
      loveIt: -1,
      created_at: new Date()
    },
    {
      title: 'Post 4',
      content: 'Je ne sais même pas pourquoi je suis là. ',
      loveIt: 2,
      created_at: new Date()
    }
  ];


  emmitPostSubject() {
    this.postsSubject.next(this.posts.slice());
  }

  doNotLovePost(post: Post) {
    post.loveIt--;
  }

  lovePost(post: Post) {
    post.loveIt++;
  }

  addPost(post: Post) {
    this.posts.push(post);
  }

  removePost(post: Post) {
    this.posts.splice(this.posts.findIndex(postObj => postObj === post), 1);
    this.emmitPostSubject();
  }

}
